"use strict";

const fs = require('fs');
const Path = require('path');

const iterator = require('batch-iterator');
const scraper = require('metascraper');

function extractMetadata(path) {
    if (!Path.isAbsolute(path)) {
        path = Path.join(process.cwd(), path);
    }
    return new Promise(function(resolve, reject) {
        var indexFile = Path.join(path, 'index.html');
        var meta = {
            path: indexFile,
            url: "",
            image: "",
            size: 0,
            modified: null,
            created: null,
            published: null,
            expires: null,
            status: "draft",
            tags: [],
            type: "article",
            author: "",
            title: ""
        };
        // First we get data about the folder itself
        // - size
        // - mtime
        // - ctime
        fs.stat(path, function(err, stats) {
            if (err) {
                reject("Path not found: " + path);
            }
            else {
                meta.size = stats.size;
                meta.modified = stats.mtime;
                meta.created = stats.ctime;
                fs.readFile(indexFile, function(err, html) {
                    if (err) {
                        resolve(meta);
                    }
                    else {
                        // Extract meta data
                        scraper.scrapeHtml(html)
                            .then(function(metadata) {
                                var now;
                                // console.log(metadata);
                                // Let's figure out status
                                // - draft [default]
                                // - published
                                // - scheduled
                                if (metadata.date) {
                                    meta.published = metadata.date;
                                    now = Date.now();
                                    if (new Date(metadata.date).getTime() <= now) {
                                        meta.status = "published";
                                    }
                                    else {
                                        meta.status = "scheduled";
                                    }
                                }
                                meta.title = metadata.title || "";
                                meta.author = metadata.author || "";
                                meta.image = metadata.image || "";
                                meta.url = metadata.url || "";
                                resolve(meta);
                            });
                    }
                });
            }
        });
    });
}

var api = {
    // Scan a folder for content
    // Will return a list of paths
    // Excludes folders beginning with _
    scan: function scan(src, next) {
        var dir = Path.resolve(process.cwd(), src); 
        var folders = fs.readdirSync(dir)
            .filter(function(file) {
                var firstChar = file.charAt(0);
                if (firstChar === '_'
                    || firstChar === '.') {
                    return false;
                }
                return fs.statSync(Path.join(dir, file)).isDirectory();
            })
            .map(function(folder) {
                return Path.join(dir, folder);
            });
        next(null, folders);
    },
    scanHidden: function scanHidden(src, next) {
        var dir = Path.resolve(process.cwd(), src); 
        var folders = fs.readdirSync(dir)
            .filter(function(file) {
                if (file.charAt(0) !== '_') return false;
                return fs.statSync(Path.join(dir, file)).isDirectory();
            })
            .map(function(folder) {
                return Path.join(dir, folder);
            });
        next(null, folders);
    },
    // Crawl a list of paths for meta-data
    // Will return a list of objects describing each item
    crawl: function crawl(paths, next) {
        iterator(paths, 50, extractMetadata)
            .then(function(contentData) {
                next(null, contentData);
            })
            .catch(function(err) {
                // console.log("Not able to crawl and extract meta data");
                next({code: 500, msg: err});
            });
    },
    // Read meta-data of one file
    read: function read(path, next) {
        extractMetadata(path)
            .then(function(meta) {
                next(null, meta);
            })
            .catch(function(err) {
                next({code:500, msg:err});
            });
    },
    // Diff current folders with provided list
    // Will return an object containing two lists
    // - "added" - folders that have been added
    // - "removed" - folders that have been removed
    diff: function diff(src, comparison, next) {
        // Get current list of folders
        api.scan(src, function(err, folders) {
            var added = folders.filter(function(item) {
                if (comparison.indexOf(item) < 0) return true;
                return false;
            });
            var removed = comparison.filter(function(item) {
                if (folders.indexOf(item) < 0) return true;
                return false;
            });
            next(null, {
                added: added,
                removed: removed
            });
        });
    },
    // Find folders that have been modified
    // Expects a list of meta data objects that have:
    // - modified
    // - path
    // TODO: use iterator to run all stats info
    modified: function modified(content, next) {
        next({code: 501, msg: "Not yet implemented"});
        // var modified = [];
        // // Extract relevant data from content list
        // var meta = content.map(function(metadata, i) {
        //     return {
        //         modified: metadata.modified,
        //         path: metadata.path,
        //         index: i
        //     };
        // });
        // meta.forEach(function(folder) {
        //     var folderPath = Path.dirname(folder.path);
        //     fs.stat(folderPath, function(err, stats) {
        //         if (Date.parse(stats.mtime) > Date.parse(folder.modified)) {
        //             modified.push(meta[folder.index]);
        //         }
        //     });
        // });
    }
};

module.exports = api;
