"use strict";

const test = require('tap').test;
const Path = require('path');

const scanner = require('../index');

test("get content folders", function(t) {
    scanner.scan('test/content', function(err, folders) {
        t.error(err);
        t.deepEqual(folders, [
            Path.join(process.cwd(), "test/content/art-revolt"),
            Path.join(process.cwd(), "test/content/back-to-italy"),
            Path.join(process.cwd(),  "test/content/nice-weather")
        ]);
        t.end();
    });
});

test("get hidden folders", function(t) {
    scanner.scanHidden('test/content', function(err, folders) {
        t.error(err);
        t.deepEqual(folders, [
            Path.join(process.cwd(), "test/content/_crafted"),
            Path.join(process.cwd(),  "test/content/_shared")
        ]);
        t.end();
    });
});

test("get diff of folders", function(t) {
    var paths = [
        Path.join(process.cwd(), "test/content/art-revolt"),
        Path.join(process.cwd(), "test/content/nice-weather"),
        Path.join(process.cwd(), "test/content/gurney-journey")
    ];
    scanner.diff('test/content', paths, function(err, diff) {
        t.error(err);
        t.ok(diff);
        t.deepEqual(diff.added, [
            Path.join(process.cwd(), "test/content/back-to-italy")
        ]);
        t.deepEqual(diff.removed, [
            Path.join(process.cwd(), "test/content/gurney-journey")
        ]);
        t.end();
    });
});

test("crawl for meta data, empty folder", function(t) {
    var folders = [
        Path.join(process.cwd(), "test/content/nice-weather")
    ];
    scanner.crawl(folders, function(err, data) {
        var content = data[0]; // We get an array
        t.error(err);
        t.ok(content.modified);
        t.ok(content.created);
        t.equal(content.published, null);
        t.equal(content.author, "");
        t.equal(content.path, Path.join(process.cwd(), "test/content/nice-weather/index.html"));
        t.equal(content.url, "");
        t.equal(content.image, "");
        t.equal(content.status, "draft");
        t.equal(content.type, "article");
        t.equal(content.title, "");
        t.end();
    });
});

test("crawl for meta data, empty input", function(t) {
    var folders = [];
    scanner.crawl(folders, function(err, data) {
        t.ok(err);
        t.equal(err.code, 500);
        t.equal(err.msg, "batch-iterator: Nothing to iterate");
        t.end();
    });
});

test("crawl for meta data, invalid input", function(t) {
    var folders = ["/kaaaargh"];
    scanner.crawl(folders, function(err, data) {
        t.ok(err);
        t.equal(err.code, 500);
        t.equal(err.msg, "Path not found: /kaaaargh");
        t.end();
    });
});

test("crawl for meta data, with index.html", function(t) {
    var folders = [
        Path.join(process.cwd(), "test/content/back-to-italy")
    ];
    scanner.crawl(folders, function(err, data) {
        var content = data[0]; // We get an array
        t.error(err);
        t.ok(content.modified);
        t.ok(content.created);
        t.ok(content.published);
        t.equal(content.author, "Dante Zerman");
        t.equal(content.path, Path.join(process.cwd(), "test/content/back-to-italy/index.html"));
        t.equal(content.url, "http://painted.ink/content/pen-drawings-of-florence");
        t.equal(content.image, "http://painted.ink/content/pen-drawings-of-florence/assets/florence_dome.jpg");
        t.equal(content.status, "published");
        t.equal(content.type, "article");
        t.equal(content.title, "Pen Drawings of Florence by Herbert Railton");
        t.end();
    });
});

test("crawl for meta data, scheduled content", function(t) {
    var folders = [
        Path.join(process.cwd(), "test/content/art-revolt")
    ];
    scanner.crawl(folders, function(err, data) {
        var content = data[0]; // We get an array
        t.error(err);
        t.ok(content.published);
        t.equal(content.status, "scheduled");
        t.end();
    });
});

test("read meta data from file, scheduled content", function(t) {
    var folder = "test/content/art-revolt";
    scanner.read(folder, function(err, data) {
        var content = data;
        t.error(err);
        t.ok(content.published);
        t.equal(content.status, "scheduled");
        t.end();
    });
});

test("read meta data from file, invalid path", function(t) {
    var folder = "/aaaaargh";
    scanner.read(folder, function(err, data) {
        var content = data;
        t.ok(err);
        t.equal(err.code, 500);
        t.equal(err.msg, "Path not found: /aaaaargh");
        t.end();
    });
});

test("find modified folders", function(t) {
    scanner.modified([], function(err, data) {
        t.equal(err.code, 501);
        t.equal(err.msg, "Not yet implemented");
        t.end();
    });
});
