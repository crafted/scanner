# Crafted Scanner
[![build status](https://gitlab.com/crafted/scanner/badges/master/build.svg)](https://gitlab.com/crafted/scanner/commits/master)
[![coverage report](https://gitlab.com/crafted/scanner/badges/master/coverage.svg)](https://gitlab.com/crafted/scanner/commits/master)

Scan for content folders and meta data in the folders.

## API

### scan(src, next)
Scan src path for folders. Will exclude any folder beginning with '_' or '.''.

### scanHidden(src, next)
Scan src path for folders beginning with '_'. Will not include folders beginning with '.'.

### diff(src, folders, next)
Scan src path and compare against list of supplied folders.
Returns an object with lists of "added" and "removed" folders from the src.

### crawl(paths, next)
Crawl the paths for meta data. Will look for an index.html file in each path.

Following meta data will be attempted:

- path
- url
- image
- size
- modified
- created
- published
- expires
- status
- tags
- type
- author
- title
